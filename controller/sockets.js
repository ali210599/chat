const db = require("../model/db");
const info = db.info;
const history = []

module.exports.IOInit = io => {
	io.on("connection", client => {


// Отправка сообщения в общем чате
client.on("send message", ({ nickname, msg, time }, callback) => {
	history.push({ nickname, msg, time })
	io.emit("get message", nickname, msg, time);
	callback();
});

// Получение никнейма для клиента, проведение проверок на валидность ника
client.on("set nickname", nickname => {
	client.emit("get nickname", nicknameError(info.users, nickname, "имя", "nickname"), nickname);
});


// Получение названия для комнаты, проведение проверок на валидность ника
client.on("set title room", (nickname, title, admin) => {
	var validNick = nicknameError(info.users, nickname, "имя", "nickname");
	var validTitle = nicknameError(info.rooms, title, "название", "title");
	if (!admin) {
		if (validNick) {
			client.emit("get title room", validNick, nickname, title);
		} else {
			client.emit("get title room", false, nickname, title);
		}
	} else {
		if (validNick || validTitle) {
			if (validTitle) { 
				client.emit("get title room", validTitle, nickname, title);
			} else {
				client.emit("get title room", validNick, nickname, title);
			}
		} else {
			if (nickname === title) {
				client.emit("get title room", {msg: "Имя и название не могут быть одинаковыми!"}, nickname, title)
			} else { 
				client.emit("get title room", false, nickname, title);
			}
		}
	}
	return false;
});

// Создание комнаты и вход в комнату
client.on("create room", (roomID, nickname, title, callback) => {
	if (!getRoomById(roomID)) {
		
		info.rooms.push({
			id: roomID,
			title,
			adminNick: nickname,
			admin: client.id,
			clients: [{
				id: client.id,
				admin: true,
				nickname
			}]
		});

		console.log(`
Создана комната ${title}
RoomID: ${roomID} 
Админ: ${nickname}`.black.bgGreen);
	} else {
		const room = info.rooms.find(r => r.id === roomID);
		
		room.clients.push({
			id: client.id,
			nickname
		});
		
		client.join(room.admin);

		console.log(`
Вход в комнату ${title}
RoomID: ${roomID}
Имя: ${nickname}
Админ: ${room.adminNick}`)			
	}

		info.users.push({
			id: client.id,
			nickname,
			room: roomID
		})

		callback();
});

// Приглашения в комнату
client.on("invite to room", ({ clientID, roomTitle, roomID, nickname }, callback) => {
	var cl = getClientById(clientID);
	if (cl) {
		client.to(cl.id).emit("invite to room", nickname, roomTitle, roomID);
		callback(false);
	} else {
		callback(!false);
	}
});

// Клиент отклонил приглашение в комнату
client.on("invite rejected", (roomID, nickname) => {
	var room = getRoomById(roomID);
	if (room) {
		client.room();
	}
});

// Приглашение в личный чат
client.on("invite to private", ({ clientID, nickname }, callback) => {
	var reply = info.users.find(u => u.nickname === nickname);
	var to = getClientById(clientID);
	if (to) {
		client.to(to.id).emit("invite to private", to.nickname, reply.id, reply.nickname);
		callback(false);
	} else {
		callback(!false);
	}
});

client.on("resolve private", (reply, privateID, nickname) => {
	client.to(reply).emit("resolve private", privateID, nickname);
});

client.on("message to private", ({ privateID, nickname, msg, time }, callback) => {
	var private = getPrivateById(privateID);
	if (private) {
		io.to(private.admin).emit("message", nickname, msg, time);
		callback();
	}
});

client.on("create private", (privateID, nickname, callback) => {
	var private = getPrivateById(privateID);
	if (!private) {
		info.private.push({
			id: privateID,
			admin: client.id,
			clients: [{
				id: client.id,
				nickname
			}]
		})
		callback();
	}
});

client.on("join in private", (privateID, nickname) => {
	var private = getPrivateById(privateID);
	if (private && private.clients.length < 2) {
		private.clients.push({
			id: client.id,
			nickname
		})
		client.join(private.admin);
		io.to(private.admin).emit("private connected", private.clients[0].nickname, nickname);
	} else {
		client.emit("private full");
	}
});

// Получение счетчиков
// Получить количество клиентов в определенной комнате
client.on("get count clients in room", roomID => {
	io.emit("get count clients in room", getRoomById(roomID).clients.length);
});
// Получить клиентов в определенной комнате
client.on("get clients in room", roomID => {
	client.emit("get clients in room", getRoomById(roomID).clients);
});
// Получить всех клиентов, которые в общем чате
client.on("get global clients", roomID => {
	client.emit("get global clients", info.users.filter(u => !("room" in u)));
});
// Получить количество всех клиентов, которые в общем чате
client.on("client count", () => {
	io.emit("client count", info.users.filter(u => !("room" in u)).length);
});
// Получить всех клиентов
client.on("get online", () => {
	client.emit("get online", info.users.filter(u => !("room" in u)));
});


// Сообщение в определенной комнате
client.on("message to room", ({ roomID, nickname, msg, time }, callback) => {
	var room = getRoomById(roomID);
	if (room) {
		io.to(room.admin).emit("message to client", roomID, nickname, msg, time);
		callback();
		return false;
	}
});

// Если комната еще не создана, то создать ее, иначе войти
client.on("check exist room", roomID => {
	var room = getRoomById(roomID)
	if (room) 
		client.emit("room exist", true, room.title);
	else
		client.emit("room exist", false);
});

// Кто-то вошел в приватный чат
client.on("new client in room", (roomID, nickname) => {
	var room = getRoomById(roomID);
	if (room) {
		client.to(room.admin).emit("new client in room", nickname, room.clients);
		return false;
	}
})

// Когда клиент вошел в общий чат!
client.on("online", (nickname, callback) => {
	info.users.push({
		id: client.id,
		nickname
	});

	client.broadcast.emit("new client", nickname);
	io.emit("client count", info.users.filter(u => !("room" in u)).length);
	callback(history);
});

// Когда клиент отключился
client.on("disconnect", () => {
	io.emit("client count", info.users.filter(u => !("room" in u)).length);
	for (let r = 0; r < info.rooms.length; r++) {
		var room = info.rooms[r]
		var index = room.clients.findIndex(cl => cl.id === client.id);
		if (index > -1) {
			room.clients.splice(index, 1);
			io.emit("get count clients in room", room.clients.length);
			if (room.clients.length < 1) {
				info.rooms.splice(r, 1);
			} else {
				room.clients[0].admin = true;
			}
			break
		}
	}

	for (let p = 0; p < info.private.length; p++) {
		let private = info.private[p]
		let index = private.clients.findIndex(cl => cl.id === client.id);
		if (index > -1) {
			client.to(private.admin).emit("private end");
			info.private.splice(p, 1);
			break;
		}
	}
});


// Когда клиент вышел, т.е закрыл вкладку, то удалить его из подключенных
client.on("client exit", nickname => {
	var index = info.users.findIndex(u => u.nickname === nickname);
	if (index > -1) {
		info.users.splice(index, 1);
		io.emit("client count", info.users.filter(u => !("room" in u)).length);
	}
	return false;
});
});}

// Вспомогательные функции \\

function getRoomById(roomID) {
	return info.rooms.find(room => room.id === roomID);
}

function getClientById(clientID) {
	return info.users.find(u => u.id === clientID);
}

function getPrivateById(privateID) {
	return info.private.find(p => p.id === privateID);
}

function nicknameError(where, nickname, placeholder, prop) {
	if (trimCleaner(nickname)) {
		return {
			msg: `Некорректное ${placeholder}, минимум 5 символов`
		}
	}

	if (where.length) {
		var item = where.find(el => el[prop] === nickname);
		if (!item) 
			return false;
		else 
			return {
				msg: `Данное ${placeholder} уже занято!`
			}
	} else return false;
}


function trimCleaner(str) {
	return !str.trim() || str.length < 5;
}