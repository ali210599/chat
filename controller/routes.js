const info = require("../model/db");
const routes = {};
const getID = str => str.substr(str.length - 5);

// Routes
routes.index = (rq, rs) => {
	rs.render('index');
}

routes.createRoom = (rq, rs) => {
	rs.redirect('/room/' + getID(Math.random() + ''));
}

routes.room = (rq, rs) => {
	rs.render('room', { id: rq.params.id });
}

routes.createPrivate = (rq, rs) => {
	let { reply } = rq.params,
			{ nick }  = rq.query;
	rs.redirect(`/private/${getID(Math.random() + '')}?reply=${reply}&nick=${nick}`);
}

routes.private = (rq, rs) => {
	rs.render("private", {id: rq.params.id});
}


module.exports = routes;