(function SocketIOModule() {
  const client = io();
  var roomID = window.location.href.match(/room\/(\d{5})/)[1];
  const info = initInfo();
  info.roomID = roomID;
  info.hash = decodeURI(window.location.hash);
  

  client.emit("check exist room", roomID);

  client.on("get count clients in room", count => {
    $("#conf-clients").html(`Собеседники: ${count}`);
  });

  client.on("client count", count => {
    $("#invite").html(`Пригласить: ${count}`);
  });

  client.on("get clients in room", clients => {
    $("#inner-clients-list").empty();
    for (var client of clients) {
      drawInnerClients($("#inner-clients-list"), {
        nickname: client.nickname,
        admin: client.admin
      }, client.nickname === info.nickname);
    }
  });

  client.on("get global clients", clients => {
    $("#outer-clients-list").empty();
    if (!clients.length) {
      $("#outer-clients-list").append(`
      <li id="inviters-none" class="collection-item">
        <h5 align="center">Никого нет в сети, чтобы пригласить.</h5>
      </li>`);
       return false;
    } 
    for (var client of clients) {
      drawOuterClients($("#outer-clients-list"), {
        nickname: client.nickname,
        id: client.id
      })
    }
  });

  client.on("new client in room", (nickname, clients) => {
    Materialize.toast(`${nickname} вошел в чат`, 4000);
  });
  
  client.on("get title room", (validTitle, nickname, title) => {
    console.log(validTitle);
    if (!validTitle) {
      client.emit("create room", info.roomID, nickname, title, () => {
        info.nickname = nickname;
        info.title = title;
        client.emit("new client in room", roomID, nickname);
        client.emit("get count clients in room", roomID);
        client.emit("client count");
        
        $("#logo").html(`ChaTOP <span id="room-title" style="font-size: 13px;">${title}<span>`);
        $("#login").remove();
        $("#messanger").fadeIn();
      });
      return false;
    }
    $("#error").remove();
    $(`<div id="error" class="card-panel red accent-4 white-text">${validTitle.msg}</div>`).insertBefore("#login-btn");
    $("#error").delay(3000).fadeOut();  
  });

  client.on("room exist", (isEx, roomTitle) => {
    if (!isEx) {
      $("#login-btn").html(`<i class="material-icons right">chevron_right</i>создать приватный чат`);
      $("#login-btn").click(e => {
        client.emit("set title room", $("#nickname").val(), $("#room-title").val(), true);
      });
    } else {
      $("#login-btn").click(e => {
        client.emit("set title room", $("#nickname").val(), roomTitle, false);
      });
      if (info.hash) {
        $("#nickname").val(info.hash.replace("#", ""));
        $("#login-btn").trigger("click");
      }
      $("#room-title").remove();
      $("#login-btn").html(`<i class="material-icons right">chevron_right</i>войти в приватный чат`);
    }     
      
  });

  client.on("message to client", (id, nickname, msg, time) => {
    if (nickname === info.nickname)
      drawMessage($("#chat-history"), {nickname, msg, time}, true);
    else
      drawMessage($("#chat-history"), {nickname, msg, time}, false);
  });

  // init
  otherLogic(client, info);
})();



function otherLogic(client, info) {
  $(".modal").modal();
  $("#msg").keydown(function (event) {
    if ($.trim($(this).val()) && event.keyCode === 13) {
      if (!$.trim($(this).val())) return false;
      client.emit("message to room", {
        roomID: info.roomID, 
        nickname: info.nickname, 
        msg: $(this).val(), 
        time: getDate()
      }, () => {
        $(this).val("");
      });
    }
  })

  $("#conf-clients").on("click", e => {
    client.emit("get clients in room", info.roomID); 
  });

  $("#invite").on("click", e => {
    client.emit("get global clients", info.roomID);
  });

  $(document).on("click", "#invite-client-btn", function(event) {
    event.preventDefault();
    var $clientID = $(this).attr("href");
    client.emit("invite to room", {
      clientID: $clientID, 
      roomTitle: info.title, 
      roomID: info.roomID, 
      nickname: info.nickname
    }, err => {
        if (!err)
          $(this).html(`<span class="secondary-content"><i class="material-icons" style="color: #880E4F;">done</i></span>`);
        else
          $(this).html(`<span class="secondary-content"><i class="material-icons" style="color: #880E4F;">error_outline</i></span>`);
    });
  });

  // Спросить при закрытии
  // window.onbeforeunload = e => {
  //   client.emit("get count clients in room", info.roomID);
  // }
}

// App state
function initInfo() {
  return {
    roomID: null
  }
}

function getDate() {
	var hours = new Date().getHours() + "";
	var minutes = new Date().getMinutes() + "";
	if (hours.length == 1)
		hours = 0 + hours;
	if (minutes.length == 1)
		minutes = 0 + minutes;

	return hours + ":" + minutes;
}

function drawMessage(chat, {nickname, msg, time}, self) {
  console.log(nickname + " => " + msg + " | " + time);
  if ($("#history-none").length) 
    $("#history-none").remove();
  
  if (!self)
    chat.prepend(`<li class="collection-item"><span class="secondary-content">${time}</span><b>${nickname}</b> ${msg}</li>`);
  else 
    chat.prepend(`<li class="collection-item pink lighten-5"><span class="secondary-content">${time}</span><b>${nickname}</b> ${msg}</li>`);
}


function drawInnerClients(list, {nickname, admin}, self) {
  if (admin) {
    if (!self) 
      list.append(`<li class="collection-item"><span class="secondary-content"><i class="material-icons right" style="color: black;">star</i></span><b>${nickname}</b></li>`);
    else
      list.append(`<li class="collection-item pink lighten-5"><span class="secondary-content"><i class="material-icons right" style="color: black;">star</i></span><b>${nickname}</b></li>`);
    return false;
  }
  if (!self)
    list.append(`<li class="collection-item"><b>${nickname}</b></li>`);
  else 
    list.append(`<li class="collection-item pink lighten-5"><b>${nickname}</b></li>`);
}


function drawOuterClients(list, {nickname, id}) {
  list.append(`<li class="collection-item"><a id="invite-client-btn" href="${id}" class="secondary-content"><i class="material-icons" style="color: #880E4F;">send</i></a><b>${nickname}</b></li>`);
}