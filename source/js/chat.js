(function SocketIOModule() {
  const client = io();

  // состояние
  const info = initInfo();


  // Обработчики клиента
  // Вошел новый клиент в общий чат
  client.on("new client", data => {
    Materialize.toast(`${data} в сети`, 4000)
  });

  // Приглашение в личный чат
  client.on("invite to private", (nickname, reply, rNickname) => {
    var $toastContent = $(`<span>Приглашение в личный чат: ${rNickname}</span>`).add($(`<a href="/create/${reply}?nick=${nickname}"><button class="btn-flat toast-action">Войти</button</a>`));
    Materialize.toast($toastContent, 10000);
  });

  // Приглашение в личный чат подтверждено
  client.on("resolve private", (privateID, nickname) => {
    console.log("NICK", nickname);
    var $toastContent = $(`<span>${nickname} принял приглашение<span>`).add($(`<a href="/private/${privateID}?nick=${info.nickname}"><button class="btn-flat toast-action">Войти</button</a>`));
    Materialize.toast($toastContent, 10000);
  });

  // Приглашение в комнату
  client.on("invite to room", (nickname, title, roomID) => {
    var $toastContent = $(`<span>Приглашение в приватный чат: ${title} от ${nickname}</span>`).add($(`<a href="/room/${roomID}#${info.nickname}"><button class="btn-flat toast-action">Войти</button</a>`));
    Materialize.toast($toastContent, 10000);
  });
  
  // Получение ника
  client.on("get nickname", (err, nickname) => {
    if (!err) {
      client.emit("online", nickname, history => {
        info.nickname = nickname;
        $("#login").remove();
        $("#messanger").fadeIn();
        drawHistory(history, nickname);
      });
      return false;
    }

    $("#error").remove();
    $(`<div id="error" class="card-panel red accent-4 white-text">${err.msg}</div>`).insertBefore("#login-btn");
    $("#error").delay(3000).fadeOut();    
  });
  
  // Получение сообщения
  client.on("get message", function (nickname, msg, time) {
    if (nickname === info.nickname)
      drawMessage($("#chat-history"), {nickname, msg, time}, true);
    else
      drawMessage($("#chat-history"), {nickname, msg, time}, false);
  });
  
  // Узнать сколько клиентов онлайн
  client.on("client count", count => {
    $("#countClient").html("Онлайн: " + (--count));
  });

  
  client.on("get online", users => {
    if (users.length < 2) {
      $("#outer-online-list").empty();
      $("#invites-none").remove();
      $("#outer-online-list").append(`
      <li id="inviters-none" class="collection-item">
        <h5 align="center">Никого нет в сети.</h5>
      </li>`);
       return false;
    } 
    onlineList(users, info);
  });

  // Private message handler
  client.on("private chat", data => {
    var $toastContent = $(`<span>Приглашение в приватный чат: ${data.nickname}</span>`).add($(`<a href="/send/${data.id}"><button class="btn-flat toast-action">Войти</button</a>`));
    Materialize.toast($toastContent, 10000);
  });


  // Инициализация
  otherLogic(client, info);
})();





// Обработчики документа, которые работают с сокетом клиента
function otherLogic(client, info) {
  $(".modal").modal();

  // Отправка сообщения по нажатию на Enter === 13
  $("#msg").keydown(function (event) {
    if ($.trim($(this).val()) && event.keyCode === 13) {
      if (!$.trim($(this).val())) return false;
      client.emit("send message", {
        nickname: info.nickname, 
        msg: $(this).val(), 
        time: getDate()
      }, () => {
        $(this).val("");
      });
    }
  })

  // Кнопка входа
  $("#login-btn").click(function (event) {
    client.emit("set nickname", $("#nickname").val());
  })

  $("#countClient").on("click", e => {
    e.preventDefault()
    client.emit("get online"); 
  });
  
  $(document).on("click", "#invite-client-btn", function(event) {
    event.preventDefault();
    var $clientID = $(this).attr("href");
    client.emit("invite to private", {
      clientID: $clientID,
      nickname: info.nickname
    }, err => {
        if (!err)
          $(this).html(`<span class="secondary-content"><i class="material-icons" style="color: #880E4F;">done</i></span>`);
        else
          $(this).html(`<span class="secondary-content"><i class="material-icons" style="color: #880E4F;">error_outline</i></span>`);
    });
  });

  // Спросить при закрытии
  window.onbeforeunload = e => {
    client.emit("client exit", info.nickname);
  }
}







// Инициализация состояния
function initInfo() {
  return {
    nickname: undefined
  }
}

// Вывести сообщение
function drawMessage(chat, {nickname, msg, time}, self) {
  // console.log(nickname + " => " + msg + " | " + time);
  if ($("#history-none").length) 
    $("#history-none").remove();
  
  if (!self)
    $("#chat-history").prepend(`<li class="collection-item"><span class="secondary-content">${time}</span><b>${nickname}</b> ${msg}</li>`);
  else 
    $("#chat-history").prepend(`<li class="collection-item pink lighten-5"><span class="secondary-content">${time}</span><b>${nickname}</b> ${msg}</li>`);
}

// Получить время
function getDate() {
	var hours = new Date().getHours() + "";
	var minutes = new Date().getMinutes() + "";
	if (hours.length == 1)
		hours = 0 + hours;
	if (minutes.length == 1)
		minutes = 0 + minutes;

	return hours + ":" + minutes;
}

// Вывести историю
function drawHistory(history, nickname) {
  // console.log("HISTORY", history.length?history:"EMPTY");
  if (!history.length) return false;
  for (var his of history) {
    drawMessage($("#chat-history"), {
      nickname: his.nickname,
      msg: his.msg,
      time: his.time
    }, his.nickname === nickname);
  }
}

function onlineList(users, info) {
  $usersOnline = $("#outer-online-list");

  if (!users.length) {
    $usersOnline.empty().append(`<li id="online-none" class="collection-item"><h5 align="center">Никого нет в онлайне!</h5></li>`);;
  } else {
    $("#online-none").remove();
    $usersOnline.empty();
    for (var user of users) {
      if (user.nickname === info.nickname) continue;
      $usersOnline.append(`<li class="collection-item"><b>${user.nickname}</b><a id="invite-client-btn" href="${user.id}" class="secondary-content"><i class="material-icons" style="color: #880E4F;">send</i></a></li>`);
    }
  }
}