(function SocketIOModule() {
  const client = io();
  const info = initInfo();
  
  var url = decodeURI(window.location.href); // Decode the url
  info.privateID = url.match(/private\/(\d{5})/)[1];
  if (url.includes("nick")) {
    info.nickname = url.match(/nick=(.+)$/)[1];
    if (url.includes("reply")) {
      let reply = url.match(/reply=(.+)&/)[1];
      client.emit("create private", info.privateID, info.nickname, () => {
        $("#logo").html(`ChaTOP <span id="room-title" style="font-size: 13px;">Ожидание собеседника<span>`);
        client.emit("resolve private", reply, info.privateID, info.nickname); 
      });
    } else {
      client.emit("join in private", info.privateID, info.nickname);
    }
  } else {
    client.emit("join in private", info.privateID, "third client");
  }

  client.on("private connected", (creator, inviter) => {
    $("#logo").html(`ChaTOP <span id="room-title" style="font-size: 13px;"><b>${creator===info.nickname?creator:inviter}</b> - ${inviter===info.nickname?creator:inviter}<span>`);
  });

  client.on("private end", () => {
    $(document.body).empty();
    $(document.body).append('<h2 align="center">Личный чат завершен!</h2>')
    $(document.body).append(`<h5 align="center"><a href="/">Общий чат</a></h5>`);
  });


  client.on("private full", () => {
    $("#messanger").empty();
    $("#messanger").append(`<h3 align="center">Это приватный чат, у вас нет доступа!</h3>`);
    $("#messanger").append(`<h5 align="center"><a href="/">Общий чат</a></h5>`);
  });
  


  client.on("message", function (nickname, msg, time) {
    if (nickname === info.nickname)
      drawMessage($("#chat-history"), {nickname, msg, time}, true);
    else
      drawMessage($("#chat-history"), {nickname, msg, time}, false);
    
  });

  // init other logic
  otherLogic(client, info);
})();


function otherLogic(client, info) {
  $("#msg").keydown(function (event) {
    if ($.trim($(this).val()) && event.keyCode === 13) {
      if (!$.trim($(this).val())) return false;
      client.emit("message to private", {
        privateID: info.privateID,
        nickname: info.nickname, 
        msg: $(this).val(),
        time: getDate()
      }, () => {
        $(this).val("");
      });
    }
  })
}



// App state
function initInfo() {
	return {
		id: null
	}
}


function drawMessage(chat, {nickname, msg, time}, self) {
  console.log(nickname + " => " + msg + " | " + time);
  if ($("#history-none").length) 
    $("#history-none").remove();
  
  if (!self)
    $("#chat-history").prepend(`<li class="collection-item"><span class="secondary-content">${time}</span><b>${nickname}</b> ${msg}</li>`);
  else 
    $("#chat-history").prepend(`<li class="collection-item pink lighten-5"><span class="secondary-content">${time}</span><b>${nickname}</b> ${msg}</li>`);
}

function getDate() {
  var hours = new Date().getHours() + "";
  var minutes = new Date().getMinutes() + "";
  if (hours.length == 1)
    hours = 0 + hours;
  if (minutes.length == 1)
    minutes = 0 + minutes;

  return hours + ":" + minutes;
}